/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;
import dn.Process;
import hxd.Key;

class PadEvent extends Process {

	public var gamepad : hxd.Pad;

	public function new(p : Process, pad: hxd.Pad) {
		super(p);
		this.gamepad = pad;
	}

	public static var MAX_BUTTON = 23;

	override function update() {
		super.update();

		for (i in 0...MAX_BUTTON) {
			if (gamepad.isPressed(i)) {
				var i = tools.CustomControl.pad(i);
				onInput(i);
			}
		}
	}

	public dynamic function onInput(k : tools.CustomControl.Input) {
	}
}

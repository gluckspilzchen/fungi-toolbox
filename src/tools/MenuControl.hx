/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;
import haxe.ds.Option;
import tools.Action;

interface MenuItem {
	public function over() : Void;
	public function out() : Void;
	public function click() : Void;
	public function logSelect() : Void;
	public function register(m : Menu) : Void;
	public function unroll() : Void;
	public function rollBack() : Void;
	dynamic public function getName() : String;
}

interface Control {
	public function update() : Void;
	public function unlock() : Void;
}

interface Menu extends MenuItem {
	public var control(default, null) : tools.CustomControl;
	public function next() : Void;
	public function back() : Void;
	public function select(mi : MenuItem) : Void;
	public function lock(c : Control) : Void;
	public function unlock() : Void;
}

abstract class MenuControl implements MenuItem implements Control implements Menu {
	public var items(default, null) : Array<MenuItem> = new Array();
	var selected : Int;
	public var control(default, null) : tools.CustomControl;

	var nextAction = Down;
	var backAction = Up;
	var unrollAction = Right;
	var rollBackAction = Left;

	public var sub : Option<Control> = None;
	public var parent : Option<Menu> = None;

	public var repl : Hake;

	private var name : String;

	dynamic public function getName() {
		return name;
	}

	public function new(c, name) {
		this.control = c;

		selected = 0;

		this.name = name;
	}

	public function update() {
		switch (sub) {
			case Some (s):
				s.update();
			case None:
				if (items.length == 0)
					return;

				if(control.probe(nextAction)) {
					next();
				}

				if(control.probe(backAction)) {
					back();
				}

				if(control.probe(Select)) {
					click();
				}

				if(control.probe(unrollAction)) {
					items[selected].unroll();
				}

				if(control.probe(rollBackAction)) {
					items[selected].rollBack();
				}

				onUpdate();
		}
	}

	dynamic public function onUpdate() {
	}

	public function addItem(i : MenuItem) {
		items.push(i);
		i.register(this);
	}

	public function removeItem(i : MenuItem) {
		items.remove(i);
	}

	public function clear() {
		items = new Array();
		selected = 0;
	}

	public function next() {
		out();
		selected++;

		if (selected >= items.length)
			selected = 0;

		over();

		onNext(selected);
	}

	dynamic public function onNext(i : Int) {
	}

	public function back() {
		out();
		selected--;

		if (selected < 0)
			selected = items.length - 1;

		over();

		onBack(selected);
	}

	dynamic public function onBack(i : Int) {
	}

	public function select(mi : MenuItem) {
		for (i in 0...items.length) {
			if (items[i] == mi) {
				selected = i;
			} else {
				items[i].out();
			}
		}

		unlock();

		switch parent {
			case Some (p): p.lock(this);
			case None:
		}

		onSelect();
	}

	dynamic public function onSelect() {
	}

	public function click() {
		items[selected].click();
	}

	public function over() {
		items[selected].over();
	}

	public function out() {
		items[selected].out();
	}

	dynamic public function onLock() {
	}

	public function lock(c : Control) {
		switch parent {
			case Some (p): p.lock(this);
			case None:
		}
		sub = Some (c);
		onLock();
	}

	dynamic public function onUnlock() {
	}

	public function unlock() {
		switch sub {
			case Some (s): s.unlock();
			case None:
		}
		sub = None;
		onUnlock();
	}

	public function unroll() {
		switch parent {
			case Some (m) : m.lock(this);
			case None :
		}
	}

	public function register(m : Menu) {
		parent = Some (m);
	}

	dynamic public function onRollBack() {
	}

	public function rollBack() {
		switch (parent) {
			case Some (m): m.unlock();
			case None:
		}

		onRollBack();
	}

	public function logSelect() {
		var options = switch (parent) {
			case Some (_):
				var item = {
					label: "Back",
					select: (function() { rollBack(); }),
				};

				[item];
			case None: [];
		}

		show(options);
	}

	public function show(firstItems : Array<tools.Hake.HakeOption>) {
		if (null == repl)
			return;

		var replItems = firstItems;

		for (i in items) {
			var item = {
				label: i.getName(),
				select: i.logSelect,
			};

			replItems.push(item);
		}

		repl.menu(replItems);
	}
}

class VerticalMenuControl extends MenuControl {

	public function new(c, name) {
		super(c, name);

		nextAction = Down;
		backAction = Up;
		unrollAction = Right;
		rollBackAction = Left;
	}
}

class HorizontalMenuControl extends MenuControl {

	public function new(c, name) {
		super(c, name);

		nextAction = Right;
		backAction = Left;
		unrollAction = Down;
		rollBackAction = Up;
	}
}

class GridMenuControl implements Menu implements Control {
	var items : Array<Array<MenuItem>> = new Array();
	var selectedX : Int;
	var selectedY : Array<Int>;
	public var control(default, null) : tools.CustomControl;

	var nextXAction = Right;
	var backXAction = Left;
	var nextYAction = Down;
	var backYAction = Up;

	public var repl : Hake;

	private var name : String;

	public var sub : Option<Control> = None;
	public var parent : Option<Menu> = None;

	public function new(c, name) {
		this.control = c;
		this.name = name;

		selectedX = 0;
		selectedY = new Array();
	}

	dynamic public function getName() {
		return name;
	}

	public function addItem(i : Int, it : MenuItem) {
		if (null == items[i]) {
			items[i] = new Array();
			selectedY[i] = 0;
		}

		items[i].push(it);
		it.register(this);
	}

	public function removeItem(i : Int, it : MenuItem) {
		items[i].remove(it);
	}

	public function clearAll() {
		items = new Array();
		selectedX = 0;
		selectedY = new Array();
	}

	public function resetSelected() {
		selectedX = 0;

		for (i in 0...selectedY.length) {
			selectedY[i] = 0;
		}
	}

	public function clear(i : Int) {
		items[i] = new Array();
		selectedY[i] = 0;
	}

	public function over() {
		items[selectedX][selectedY[selectedX]].over();
	}

	public function out() {
		items[selectedX][selectedY[selectedX]].out();
	}

	public function logSelect() {
		if (repl == null)
			return;

		var replItems = [];

		for (i in items) {
			for (j in i) {
				var item = {
					label: j.getName(),
					select: j.logSelect,
				};

				replItems.push(item);
			}
		}

		repl.menu(replItems);
	}

	public function click() {
		items[selectedX][selectedY[selectedX]].click();
	}

	public function register(m : Menu) {
		parent = Some (m);
	}

	public function update() {
		switch (sub) {
			case Some (s):
				s.update();
			case None:
				if (items.length == 0 || items[selectedX].length == 0)
					return;

				if(control.probe(Select)) {
					click();
				}

				if(control.probe(nextXAction)) {
					nextX();
				}

				if(control.probe(backXAction)) {
					backX();
				}

				if(control.probe(nextYAction)) {
					nextY();
				}

				if(control.probe(backYAction)) {
					backY();
				}
		}
	}

	public function nextX() {
		out();
		selectedX++;

		if (selectedX >= items.length)
			selectedX = 0;

		over();
	}

	public function next() { nextX(); }

	public function backX() {
		out();
		selectedX--;

		if (selectedX < 0)
			selectedX = items.length - 1;

		over();
	}

	public function back() { backX(); }

	public function nextY() {
		out();
		selectedY[selectedX]++;

		if (selectedY[selectedX] >= items[selectedX].length)
			selectedY[selectedX] = 0;

		over();
	}

	public function backY() {
		out();
		selectedY[selectedX]--;

		if (selectedY[selectedX] < 0)
			selectedY[selectedX] = items[selectedX].length - 1;

		over();
	}

	public function select(mi : MenuItem) {
		for (rows in items.keyValueIterator()) {
			for (i in rows.value.keyValueIterator()) {
				if (i.value == mi) {
					selectedX = rows.key;
					selectedY[selectedX] = i.key;
				} else {
					i.value.out();
				}
			}
		}
	}

	public function lock(c : Control) {
		sub = Some (c);
	}

	public function unlock() {
		sub = None;
	}


	public function unroll() {
		switch parent {
			case Some (m) : m.lock(this);
			case None :
		}
	}

	public function rollBack() {
		switch (parent) {
			case Some (m): m.unlock();
			case None:
		}
	}
}

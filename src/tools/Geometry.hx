/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;
import haxe.ds.Option;

enum Shape {
	Circle (cx : Float, cy : Float, r : Float);
	Segment (x1 : Float, y1 : Float, x2 : Float, y2 : Float);
}

class Geometry {
	static private function getDiscriminant(r : Float, dr : Float, det : Float) {
		return r*r*dr*dr-det*det;
	}

	static public function hasCircleLineIntersection(x : Float, y : Float, dx : Float, dy : Float, cx : Float, cy : Float, r : Float) {
		var dr = M.dist(0, 0, dx, dy);
		var x1 = x-cx;
		var y1 = y-cy;
		var x2 = x-cx+dx;
		var y2 = y-cy+dy;
		var det = x1 * y2 - x2 * y1;

		var dis = getDiscriminant(r, dr, det);
		return dis >= 0;
	}

	static public function getCircleLineIntersection(x : Float, y : Float, dx : Float, dy : Float, cx : Float, cy : Float, r : Float) {
		var dr = M.dist(0, 0, dx, dy);
		var x1 = x-cx;
		var y1 = y-cy;
		var x2 = x-cx+dx;
		var y2 = y-cy+dy;
		var det = x1 * y2 - x2 * y1;

		var dis = getDiscriminant(r, dr, det);

		var sdy = (x < -1) ? -1 : 1;

		if (dis >= 0) {
			var ix1 = ( det * dy + sdy * dx * Math.sqrt(dis)) / (dr*dr);
			var iy1 = (-det * dx + Math.abs(dy) * Math.sqrt(dis)) / (dr*dr);

			var ix2 = ( det * dy - sdy * dx * Math.sqrt(dis)) / (dr*dr);
			var iy2 = (-det * dx - Math.abs(dy) * Math.sqrt(dis)) / (dr*dr);

			if (ix1 == ix2 && iy1 == iy2)
				return Some (new h2d.col.Point(ix1+cx, iy1+cy));
			else {
				if (M.dist(x1, y1, ix1, iy1) < M.dist(x1, y1, ix2, iy2))
					return Some (new h2d.col.Point(ix1+cx, iy1+cy));
				else
					return Some (new h2d.col.Point(ix2+cx, iy2+cy));
			}
		} else return None;
	}

	/* The 3 points must be on the same line */
	static public function isInSegment(x1 : Float, y1 : Float, x2 : Float, y2 : Float, cx : Float, cy : Float) {
		var vx1 = x1 - cx;
		var vy1 = y1 - cy;

		var vx2 = x2 - cx;
		var vy2 = y2 - cy;

		var signX1 = vx1/Math.abs(vx1);
		var signX2 = vx2/Math.abs(vx2);

		var signY1 = vy1/Math.abs(vy1);
		var signY2 = vy2/Math.abs(vy2);

		return (vx1 != 0 && vx2 != 0 && signX1 != signX2) || (vy1 != 0 && vy2 !=0 && signY1 != signY2) || (x1 == cx && y1 == cy) || (x2 == cx && y2 ==cy) ;
	}

	static public function hasSegmentCircleIntersection(x1 : Float, y1 : Float, x2 : Float, y2 : Float, cx : Float, cy : Float, r : Float) {
		var dx = x2-x1;
		var dy = y2-y1;

		if (hasPointShapeIntersection(x1, y1, Circle(cx, cy, r)) || hasPointShapeIntersection(x2, y2, Circle(cx, cy, r))) {
			return true;
		}

		switch (getCircleLineIntersection(x1, y1, dx, dy, cx, cy, r)) {
			case Some (p) :
				if (isInSegment(x1, y1, x2, y2, p.x, p.y))
					return true;
			case None:
		}

		switch (getCircleLineIntersection(x2, y2, dx, dy, cx, cy, r)) {
			case Some (p) :
				return isInSegment(x1, y1, x2, y2, p.x, p.y);
			case None:
				return false;
		}
	}

	static public function hasCircleCircleIntersection(cx1 : Float, cy1 : Float, r1 : Float, cx2 : Float, cy2 : Float, r2 : Float) {
		var dist = M.distSqr(cx1, cy1, cx2, cy2);

		return dist < (r1 + r2) * (r1 + r2);
	}

	static public function hasSegmentSegmentIntersection( ax1 : Float, ay1 : Float, ax2 : Float, ay2 : Float, bx1 : Float, by1 : Float, bx2 : Float, by2 : Float ) {
		//TODO implement
		return false;
	}

	static public function hasShapeShapeIntersection (s1 : Shape, s2 : Shape) {
		return switch [s1, s2] {
			case [ Circle (cx1, cy1, r1), Circle (cx2, cy2, r2) ]: hasCircleCircleIntersection(cx1, cy1, r1, cx2, cy2, r2);
			case [ Circle (cx1, cy1, r1), Segment ( x1, y1, x2, y2 ) ]: hasSegmentCircleIntersection(x1, y1, x2, y2, cx1, cy1, r1);
			case [ Segment ( x1, y1, x2, y2 ), Circle (cx1, cy1, r1) ]: hasSegmentCircleIntersection(x1, y1, x2, y2, cx1, cy1, r1);
			case [ Segment ( ax1, ay1, ax2, ay2 ), Segment ( bx1, by1, bx2, by2 ) ]: hasSegmentSegmentIntersection( ax1, ay1, ax2, ay2, bx1, by1, bx2, by2 );
			default: false;
		}
	}

	static public function hasPointShapeIntersection(x : Float, y : Float, s : Shape) {
		return switch s {
			case Circle (cx1, cy1, r1): hasCircleCircleIntersection(cx1, cy1, r1, x, y, 0);
			case Segment ( ax1, ay1, ax2, ay2 ): isInSegment( ax1, ay1, ax2, ay2, x, y );
			default: false;
		}
	}
}

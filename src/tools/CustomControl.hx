/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;
import hxd.Key;
import tools.Action;
import haxe.ds.Option;

enum InputType {
	Keyboard;
	Pad;
}

typedef Input = {
	var code : Int;
	var type : InputType;
}

typedef KeyBinding = {
	var modifiers : Array<Input>;
	var key : Input;
	var protected : Bool;
}

abstract class CustomControl {
	static private var conflicts : Map<KeyBinding, Array<KeyBinding>> = new Map();
	static private var candidates : Map<Int, Array<KeyBinding>> = new Map();
	static public var bindings(default, null) : Map<Action, Array<KeyBinding>> = new Map();

	static public var gamepad : hxd.Pad = null;

	static var suspendEnd = 0.;

	static public var deadzone(get, set) : Float;
	static private var deadzone_v = 0.2;
	inline static function get_deadzone() return deadzone_v;
	inline static function set_deadzone(v : Float) {
		if (null != gamepad) {
			gamepad.axisDeadZone = v;
		}

		return deadzone_v = v;
	}

	public function new() {
	}

	static public function key(k : Int) {
		return { code : k, type : Keyboard };
	}

	static public function pad(c : Int) {
		return { code : c, type : Pad };
	}

	static public function init() {
		var onPad = function onPad( p : hxd.Pad ){
			trace(p.name + " connected.");
			gamepad = p;
		}
		hxd.Pad.wait(onPad);

		addBinding(Up, { modifiers : new Array(), key : key(Key.UP), protected : true });
		addBinding(Up, { modifiers : new Array(), key : pad(hxd.Pad.DEFAULT_CONFIG.analogY), protected : false });
		addBinding(Up, { modifiers : new Array(), key : key(Key.Z), protected : false });
		addBinding(Up, { modifiers : new Array(), key : key(Key.W), protected : false });

		addBinding(Down, { modifiers : new Array(), key : key(Key.DOWN), protected : true });
		addBinding(Down, { modifiers : new Array(), key : key(Key.S), protected : false });

		addBinding(Left, { modifiers : new Array(), key : key(Key.LEFT), protected : true });
		addBinding(Left, { modifiers : new Array(), key : key(Key.A), protected : false });
		addBinding(Left, { modifiers : new Array(), key : key(Key.Q), protected : false });

		addBinding(Right, { modifiers : new Array(), key : key(Key.RIGHT), protected : true });
		addBinding(Right, { modifiers : new Array(), key : pad(hxd.Pad.DEFAULT_CONFIG.analogX), protected : false });
		addBinding(Right, { modifiers : new Array(), key : key(Key.D), protected : false });

		addBinding(Select, { modifiers : new Array(), key : key(Key.ENTER), protected : true });

		addBinding(Exit, { modifiers : new Array(), key : key(Key.ESCAPE), protected : true });
	};

	static public function addBinding(a : Action, b : KeyBinding) {
		var binding = bindings.get(a);

		if (binding == null)	
			binding = new Array();

		var isPresent = false;

		var isEqual = function (cb : KeyBinding) {
			var modifiers = function () return cb.modifiers.filter(function (k : Input) return b.modifiers.contains(k));
			return cb.key.code == b.key.code && cb.key.type == b.key.type && modifiers().length == b.modifiers.length;
		}

		for (ib in binding) {
			if (isEqual(ib)) {
				isPresent = true;
				break;
			}
		}

		if (isPresent)
			return;

		binding.push(b);
		bindings.set(a, binding);

		var keyConflicts : Array<KeyBinding> = candidates.get(b.key.code);

		if (keyConflicts == null) {
			keyConflicts = new Array();
			candidates.set(b.key.code, keyConflicts);
		}

		keyConflicts.push(b);

		var isConflict = function (b1 : KeyBinding, b2 : KeyBinding) {
			var modifiers = b1.modifiers.filter(function (k : Input) return !b2.modifiers.contains(k));
			return b1 != b2 && modifiers.length == 0;
		}

		var actualConflicts : Array<KeyBinding> = keyConflicts.filter((kb) -> isConflict(b, kb));

		conflicts.set(b, actualConflicts);

		var conflictsOf = keyConflicts.filter((kb -> isConflict(kb, b)));

		for (bi in conflictsOf) {
			var actualConflicts = conflicts.get(bi);
			actualConflicts.push(b);
		}
	}

	static public function removeBinding(a : Action, b : KeyBinding){
		var binding = bindings.get(a);

		if (binding == null)	
			return;

		var isEqual = function (cb : KeyBinding) {
			var modifiers = function () return cb.modifiers.filter(function (k : Input) return b.modifiers.contains(k));
			return !cb.protected && cb.key.code == b.key.code && cb.key.type == b.key.type && modifiers().length == b.modifiers.length;
		}

		var nbinding = binding.filter((b) -> !isEqual(b));

		bindings.set(a, nbinding);

		var keyConflicts = candidates.get(b.key.code);
		keyConflicts.remove(b);

		conflicts.remove(b);

		for (cb in keyConflicts) {
			var cs = conflicts.get(cb);

			if(cs != null) {
				cs.remove(b);
			}
		}
	}

	private function probeAux(a : Action, keyPredicate : (Input) -> Bool, modifierPredicate : (Input) -> Bool) {
		var binding = bindings.get(a);
		
		if (binding == null)	
			return false;

		var isActive = function (kb : KeyBinding) {
			var modifiers = function () return kb.modifiers.filter(function (k : Input) return modifierPredicate(k));
			return keyPredicate(kb.key) && modifiers().length == kb.modifiers.length;
		};

		var hasConflict = function (kb : KeyBinding) {
			var conflicts = conflicts.get(kb);

			if (conflicts == null)
				return false;

			var hasConflict = false;

			for (b in conflicts) {
				hasConflict = hasConflict || isActive(b);
			}

			return hasConflict;
		};

		var actives = binding.filter(function (b) { return isActive(b) && !hasConflict(b); });

		var isActive = actives.length > 0;

		if (isActive)
			suspend(getSuspendDuration());

		return isActive;
	}

	private function cprobeAux(a : Action, getInputValue : (Input) -> Float, modifierPredicate : (Input) -> Bool) {
		var binding = bindings.get(a);

		if (binding == null)
			return None;

		var isActive = function (kb : KeyBinding) {
			var modifiers = function () return kb.modifiers.filter(function (k : Input) return modifierPredicate(k));
			return isDown(kb.key) && modifiers().length == kb.modifiers.length;
		};

		var hasConflict = function (kb : KeyBinding) {
			var conflicts = conflicts.get(kb);

			if (conflicts == null)
				return false;

			var hasConflict = false;

			for (b in conflicts) {
				hasConflict = hasConflict || isActive(b);
			}

			return hasConflict;
		};

		var actives = new Array();

		for (b in binding) {
			if (isActive(b) && !hasConflict(b)) {
				actives.push(getInputValue(b.key));
			}
		}

		var isActive = actives.length > 0;

		return isActive ? Some (actives[0]) : None;
	}

	abstract function getSuspendDuration() : Float;

	private function isDown(k : Input) {
		return k.type == Keyboard && Key.isDown(k.code)
			|| k.type == Pad && null != gamepad && (gamepad.isDown(k.code) || M.fabs(gamepad.values[k.code]) > deadzone);
	}

	private function isPressed(k : Input) {
		return k.type == Keyboard && Key.isPressed(k.code)
			|| k.type == Pad && null != gamepad && gamepad.isPressed(k.code);
	}

	private function getInputValue(k : Input) {
		return
			switch k.type {
				case Keyboard : Key.isDown(k.code) ? 1. : 0.;
				case Pad if (gamepad == null) : 0.;
				case Pad : gamepad.values[k.code];
			}
	}

	public function probe(a : Action) {
		return (haxe.Timer.stamp() > suspendEnd) && probeAux(a, isPressed, isDown);
	}

	public function cprobe(a : Action) {
		if (haxe.Timer.stamp() < suspendEnd)
			return None;

		return cprobeAux(a, getInputValue, isDown);
	}

	static public function suspend(d : Float) {
		var oldTs = suspendEnd;
		var newTs = haxe.Timer.stamp() + d;

		if (newTs > oldTs) {
			suspendEnd = newTs;
		}
	}

	public static function getKeyName(k : Input) {
		return
			switch (k.type) {
				case Keyboard: hxd.Key.getKeyName(k.code);
				case Pad: hxd.Pad.DEFAULT_CONFIG.names[k.code];
			}
	}

	abstract public function getActionName(a : Action) : String;
}

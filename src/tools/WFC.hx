/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;

import haxe.ds.Option;

enum Location {
	TL;
	T;
	TR;
	L;
	R;
	BL;
	B;
	BR;
}

class Neighbours {
	public var topLeft(default, null) : IntSet;
	public var top(default, null) : IntSet;
	public var topRight(default, null) : IntSet;
	public var left(default, null) : IntSet;
	public var right(default, null) : IntSet;
	public var bottomLeft(default, null) : IntSet;
	public var bottom(default, null) : IntSet;
	public var bottomRight(default, null) : IntSet;

	private function new() {
	}

	static public function empty() : Neighbours {
		var i = new Neighbours();

		i.topLeft = new IntSet();
		i.top = new IntSet();
		i.topRight = new IntSet();
		i.left = new IntSet();
		i.right = new IntSet();
		i.bottomLeft = new IntSet();
		i.bottom = new IntSet();
		i.bottomRight = new IntSet();

		return i;
	}

	static public function full(n) : Neighbours {
		var i = empty();

		var ts = [for (i in 0...n) i];

		i.topLeft.addList(ts);
		i.top.addList(ts);
		i.topRight.addList(ts);
		i.left.addList(ts);
		i.right.addList(ts);
		i.bottomLeft.addList(ts);
		i.bottom.addList(ts);
		i.bottomRight.addList(ts);

		return i;
	}

	public function merge(tc : Neighbours) {
		topLeft.merge(tc.topLeft);
		top.merge(tc.top);
		topRight.merge(tc.topRight);
		left.merge(tc.left);
		right.merge(tc.right);
		bottomLeft.merge(tc.bottomLeft);
		bottom.merge(tc.bottom);
		bottomRight.merge(tc.bottomRight);
	}
}

enum Cell {
	Unresolved(tiles : tools.IntSet);
	Resolved(id : Int);
}

typedef Tile<T> = {
	var neighbours : Neighbours;
	var priority : Int;
	var kind : T;
}

typedef Compatible<T> = {
	public function isCompatible(e : T) : Bool;
}

class WFC<B, T : Compatible<B>> {
	var height : Int;
	var width : Int;
	public var seed : Int;
	public var baseGrid(default, null) : Array<B>;
	var grid : Array<Cell>;

	var tiles : Array<Tile<T>>;
	var alternatives : Array<List<Int>>;
	var first : Int;

	var isComplete : Bool;
	var shouldRestart : Bool;

	public function new(h, w, ts, def) {
		seed = 0;
		tiles = ts;
		height = h;
		width = w;

		baseGrid = new Array();
		for (i in 0...width) {
			for (j in 0...height) {
				baseGrid.push(def);
			}
		}
	}

	function init() {
		isComplete = false;
		shouldRestart = false;
		first = tiles.length;

		alternatives = new Array();
		for (i in tiles.keyValueIterator()) {
			alternatives[i.key] = new List();
		}

		grid = new Array();
		for (i in baseGrid.keyValueIterator()) {
			var allTiles = getCandidatesFromKind(i.value);
			var c =	Unresolved (allTiles);

			addAlt(allTiles.cardinal, i.key);
			grid[i.key] = c;
		}
	}

	function removeAlt(cardinal, v) {
		var idx = cardinal - 1;
		alternatives[idx].remove(v);

		if (idx == first && alternatives[idx].isEmpty()) {
			for (i in first...alternatives.length) {
				if (!alternatives[i].isEmpty()) {
					first = i;
					return;
				}
			}
			isComplete = true;
		}
	}

	function addAlt(cardinal, v) {
		var idx = cardinal - 1;
		alternatives[idx].add(v);

		if(idx < first) {
			first = idx;
		}
	}

	function reduceCell(i, cs) {

		var candidates : IntSet;

		switch (grid[i]) {
			case Resolved (_): return;
			case Unresolved (cs): candidates = cs;
		}

		var cardinal = candidates.cardinal;

		candidates.inter(cs);

		if (candidates.cardinal == cardinal) {
			return;
		} else if (candidates.cardinal == 0) {
			shouldRestart = true;
			return;
		} else {
			var cell =
				if (candidates.cardinal == 1) {
					Resolved (candidates.choose());
				} else {
					addAlt(candidates.cardinal, i);
					Unresolved (candidates);
				}

			grid[i] = cell;

			removeAlt(cardinal, i);

			var constraints = getAggregateConstraints(i);

			reduceNeighbours(i, constraints);
		}
	}

	function getCoord(i) {
		var y = Std.int(i/width);
		var x = i - y * width;

		return [x, y];
	}

	function getNeighbour(i, loc) {
		var coord = getCoord(i);
		var x = coord[0];
		var y = coord[1];

		switch (loc) {
			case TL: x--; y--;
			case  T: y--;
			case TR: x++; y--;
			case  L: x--;
			case  R: x++;
			case BL: x--; y++;
			case  B: y++;
			case BR: x++; y++;
		};

		if (x < 0 || y < 0 || x >= width || y >= height) {
			return None;
		} else {
			return Some (x + y * width);
		}
	}

	function reduceNeighbour(idx, loc, constraints) {
		var i = getNeighbour(idx, loc);

		switch i {
			case Some (ni): reduceCell(ni, constraints);
			case None:
		}
	}

	function reduceNeighbours(i, constraints) {
		reduceNeighbour(i, TL, constraints.topLeft);
		reduceNeighbour(i,  T, constraints.top);
		reduceNeighbour(i, TR, constraints.topRight);
		reduceNeighbour(i,  L, constraints.left);
		reduceNeighbour(i,  R, constraints.right);
		reduceNeighbour(i, BL, constraints.bottomLeft);
		reduceNeighbour(i,  B, constraints.bottom);
		reduceNeighbour(i, BR, constraints.bottomRight);
	}

	function getAggregateConstraints(i) {
		var candidates =
			switch (grid[i]) {
				case Resolved (i) :
					var s = new IntSet();
					s.add(i);
					s;
				case Unresolved (cs) :
					cs;
			}
		var constraints = Neighbours.empty();
		for (i in candidates) {
			constraints.merge(tiles[i].neighbours);
		}

		return constraints;
	}

	function copyGrid(grid : Array<Cell>) {
		var copy = new Array();
		for (c in grid) {
			var nc = switch c {
				case Unresolved (cs): Unresolved (cs.copy());
				case Resolved (i): Resolved (i);
			}
			copy.push(nc);
		}

		return copy;
	}

	function copyAlternatives(alternatives : Array<List<Int>> ) {
		var copy = new Array();
		for (l in alternatives) {
			copy.push(ListUtils.copyReverse(l));
		}

		return copy;
	}

	function resolve(i : Int) {
		var savedGrid = copyGrid(grid);
		var savedAlternatives =	copyAlternatives(alternatives);
		var savedFirst = first;

		switch (grid[i]) {
			case Unresolved (candidates):
				var sorted = candidates.toArray();
				sorted.sort((i1, i2) -> {
						var p = tiles[i2].priority - tiles[i1].priority;

						if (p != 0) {
							return p;
						}

						var v1 = M.randSeedCoords(seed + i1, i % width, Math.floor(i / width), 100);
						var v2 = M.randSeedCoords(seed + i2, i % width, Math.floor(i / width), 100);

						return v2 - v1;
				});
				for (id in sorted) {
					shouldRestart = false;

					grid[i] = Resolved (id);
					removeAlt(candidates.cardinal, i);
					reduceNeighbours(i, tiles[id].neighbours);

					if (!shouldRestart)
						break;

					grid = copyGrid(savedGrid);
					alternatives = copyAlternatives(savedAlternatives);
					first = savedFirst;
					isComplete = false;
				}
			case Resolved (_):
		}
	}

	public function run() : Array<Int> {
		init();

		while (!(isComplete || shouldRestart)) {
			var candidates = alternatives[first];
			var idx = tools.ListUtils.choose(candidates);
			resolve(idx);
		}

		if (shouldRestart) {
			run();
		}

		var ret = new Array();
		for (e in grid) {
			switch e {
				case Resolved (i): ret.push(i);
				case Unresolved (_):
			}
		}

		return ret;
	}

	function getCandidatesFromKind(k) {
		var candidates = new IntSet();
		for (i in tiles.keyValueIterator()) {
			if (i.value.kind.isCompatible(k)) {
				candidates.add(i.key);
			}
		}

		return candidates;
	}

	public function setTile(x, y, k) {
		var idx = x + width * y;
		baseGrid[idx] = k;
	}

	public function setTileAndRun(x, y, k) {
		setTile(x, y, k);

		return run();
	}
}

/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;

class ListUtils {
	static public function choose<T>(l : List<T>) : T {
		var idx = Std.int(Math.random() * l.length);

		for (i in l.keyValueIterator()) {
			if (i.key == idx) {
				return i.value;
			}
		}

		return null;
	}

	static public function copyReverse<T>(l : List<T>) : List<T> {
		var nl = new List();
		for (v in l) {
			nl.push(v);
		}

		return nl;
	}
}

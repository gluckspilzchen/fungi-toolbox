/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024 Quentin Lambert
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package tools;

import haxe.ds.Option;

class IntSetIterator {
	final data : IntSet;
	var current = 0;
	var cnt : Int;

	public function new(s : IntSet) {
		data = s;
		cnt = s.cardinal;
	}

	public function hasNext() {
		return cnt > 0;
	}

	public function next() {
		for (i in current...data.elements.length) {
			if (data.elements[i]) {
				current = i+1;
				cnt --;
				break;
			}
		}

		return current-1;
	}
}

class IntSet {
	public var elements(default, null) : Array<Bool>;

	public var cardinal(default, null) : Int;

	public function new() {
		elements = new Array();
		cardinal = 0;
	}

	public function inter(s : IntSet) {
		for (i in 0...elements.length) {
			if (elements[i] && !s.elements[i]) {
				remove(i);
			}
		}
	}

	public function merge(s : IntSet) {
		for (i in 0...s.elements.length) {
			if (s.elements[i]) {
				add(i);
			}
		}
	}

	public function remove(i : Int) {
		if (!elements[i])
			return;

		elements[i] = false;
		cardinal--;
	}

	public function add(i : Int) {
		if (elements[i])
			return;

		elements[i] = true;
		cardinal++;
	}

	public function choose() : Int {
		var idx = Std.int(Math.random() * cardinal);
		for(i in 0...elements.length){
			if (!elements[i]) {
				continue;
			}

			if (idx == 0) {
				return i;
			} else {
				idx--;
			}
		}

		return -1;
	}

	public function addList(es : Array<Int>) {
		for (i in es) {
			add(i);
		}
	}

	public function iterator() {
		return new IntSetIterator(this);
	}

	public function copy() {
		var s = new IntSet();
		s.elements = elements.copy();
		s.cardinal = cardinal;

		return s;
	}

	public function contains(i : Int) {
		return elements[i];
	}

	public function filter(p : (Int) -> Bool) {
		var s = new IntSet();

		for (i in elements.keyValueIterator()) {
			if (i.value && p(i.key)) {
				s.add(i.key);
			}
		}

		return s;
	}

	public function toArray() : Array<Int> {
		var values = new Array();
		for ( i in elements.keyValueIterator()) {
			if (i.value) {
				values.push(i.key);
			}
		}

		return values;
	}
}

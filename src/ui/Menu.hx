/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

typedef Item = {
	var name : String;
	var onClick : () -> Void;
}

class Menu {
	public static function vertical(data : UISettings, items : Array<Item>) {
		var menu = data.menu;
		var name = data.name;
		var getTiles = data.getWidgetSettings;
		var root = data.root;
		var container = new h2d.Flow(root);
		container.layout = Vertical;

		var nmenu = new tools.MenuControl.VerticalMenuControl(menu.control, name);

		var remove = () -> {
			nmenu.rollBack();
			container.remove();
		}

		nmenu.onUpdate = () -> {
			if(nmenu.control.probe(Exit)) {
				remove();
			}
		}
		nmenu.register(menu);

		for (i in items) {
			var settings = getTiles();
			var b = new ui.Components.ButtonComp(settings, container);
			b.label = i.name;
			b.setVerticalMargin(5);
			b.onClick = () -> { remove(); i.onClick(); };

			nmenu.addItem(b);
		}

		nmenu.unroll();
		nmenu.over();

		container.enableInteractive = true;
		container.interactive.propagateEvents = true;
		container.interactive.onFocusLost = (_) -> remove();

		return container;
	}
}

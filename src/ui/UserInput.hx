/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

import ui.Components;

class UserInput {
	static public function readString(data : UISettings, onFocusLost : (String) -> Void) {
		var container = new h2d.Flow(data.root);
		container.layout = Vertical;
		var settings = data.getWidgetSettings();

		if (data.name != null) {
			var l = new h2d.Text(hxd.res.DefaultFont.get(), container);
			l.text = data.name;
		}

		var textField = new ui.TextFieldComp(data.menu.control, settings, container);
		var remove = () -> {
			textField.rollBack();
			container.remove();
		}
		textField.onFocusLost = (text) -> {
			remove();

			if(data.menu.control.probe(Select)) {
				onFocusLost(text);
			}
		};
		textField.register(data.menu);
		textField.click();

		return container;
	}
}

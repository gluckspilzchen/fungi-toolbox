/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

import haxe.ds.Option;

enum GaugeKind {
	Vertical;
	Horizontal;
}

typedef GaugeSettings = {
	var width : Int;
	var height : Int;
	var kind : GaugeKind;
	var ratio : Float;
	var bgColor : Int;
	var fgColor : Int;
}

class Gauge extends h2d.Object {
	public var width(default, null) : Float;
	public var height(default, null) : Float;

	var bgColor : Int;
	var fgColor : Int;

	var kind : GaugeKind;

	public var ratio(never, set) : Float;
	function set_ratio(v : Float) {
		var ret = (ratio_v = v);
		redraw();
		return ret;
	}
	var ratio_v : Float;

	public function new(s : GaugeSettings, ?parent ) {
		super(parent);

		width = s.width;
		height = s.height;

		bgColor = s.bgColor;
		fgColor = s.fgColor;

		ratio_v = s.ratio;

		kind = s.kind;

		redraw(); 
	}

	function drawHorizontal() {
		var g = new h2d.Graphics(this);
		g.beginFill(fgColor, 1.);
		g.drawRect(0, 0, ratio_v * width, height);
		g.endFill();
	}

	function drawVertical() {
		var g = new h2d.Graphics(this);
		g.beginFill(fgColor, 1.);
		g.drawRect(0, (1 - ratio_v) * height, width, ratio_v * height);
		g.endFill();
	}

	function redraw() {
		this.removeChildren();

		var g = new h2d.Graphics(this);
		g.beginFill(bgColor, 1.);
		g.lineStyle(3, bgColor);
		g.drawRect(0, 0, width, height);
		g.endFill();

		switch kind {
			case Vertical: drawVertical();
			case Horizontal: drawHorizontal();
		}
	}

	public function resize(w, h) {
		this.width = w;
		this.height = h;

		redraw();
	}
}

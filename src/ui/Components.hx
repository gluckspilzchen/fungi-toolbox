/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package ui;

import haxe.ds.Option;

typedef WidgetSettings = {
	var width : Int;
	var height : Int;
	var hover : Array<h2d.SpriteBatch.BatchElement>;
	var active : h2d.SpriteBatch;
	var base : h2d.SpriteBatch;
}

@:uiComp("interactive-widget")
class InteractiveWidgetComp extends h2d.Object implements tools.MenuControl.MenuItem implements h2d.domkit.Object {

	public var width(default, null) : Float;
	public var height(default, null) : Float;

	public var marginUp : Float;
	public var marginDown : Float;
	public var marginLeft : Float;
	public var marginRight : Float;

	var activeTile : h2d.SpriteBatch;
	var baseTile : h2d.SpriteBatch;
	var hoverTile : Array<h2d.SpriteBatch.BatchElement>;

	var background : h2d.Object;
	var currentBatch : h2d.SpriteBatch;
	var boundTile : h2d.Bitmap;
	var interactive : h2d.Interactive;

	var menu : Option<tools.MenuControl.Menu> = None;

	public var propagateEvents(get, set) : Bool;
	inline function get_propagateEvents() return interactive.propagateEvents;
	inline function set_propagateEvents(b : Bool) return interactive.propagateEvents = b;

	static var SRC = <interactive-widget>
		<text public id="labelTxt" />
	</interactive-widget>


	public var label(get, set): String;
	function get_label() return labelTxt.text;
	function set_label(s) {
		labelTxt.text = s;
		updateText();
		return s;
	}

	dynamic public function getName() {
		return label;
	}

	public function new( t: WidgetSettings, ?parent ) {
		super(parent);

		activeTile = t.active;
		hoverTile = t.hover;
		baseTile = t.base;

		currentBatch = baseTile;

		background = new h2d.Object(this);
		background.addChild(baseTile);

		initComponent();

		labelTxt = new h2d.Text(Settings.uiFont, this);

		interactive = new h2d.Interactive(t.width, t.height, this);
		
		interactive.onClick = (_) -> click();
		interactive.onPush = (_) -> push();
		interactive.onRelease = (_) -> release();
		interactive.onOut = (_) -> out();
		interactive.onOver = (_) -> over();
		interactive.onKeyDown = (e) -> keyDown(e.keyCode);

		width = t.width;
		height = t.height;

		syncScale();

		marginUp = 0;
		marginDown = 0;
		marginLeft = 0;
		marginRight = 0;

		var bTile =
			h2d.Tile.fromColor(
					0xffffff,
					Std.int(width + marginLeft + marginRight),
					Std.int(height + marginUp + marginDown),
					0.);

		boundTile = new h2d.Bitmap(bTile, this);
	}

	public function click() {
		onClick();
	}

	public function push() {
		dom.active = true;
		showActive();
		onPush();
	}

	public function release() {
		dom.active = false;
		showBase();
	}

	public function out() {
		dom.hover = false;
		hideHover();
		onOut();
	}

	public function over() {
		switch menu {
			case Some (m): m.select(this);
			case None:
		}
		dom.hover = true;
		showHover();
		onOver();
	}

	public function keyDown(k : Int) {
		onKeyDown(k);
	}

	public function unroll() {
		onUnroll();
	}

	public function register(m : tools.MenuControl.Menu) {
		this.menu = Some (m);
	}

	public function logSelect() {
		click();
	}

	public function rollBack() {
	}

	private function syncScale() {
		var b = activeTile.getBounds();
		activeTile.scale(width/b.width);
		b = baseTile.getBounds();
		baseTile.scale(width/b.width);
	}

	public dynamic function onOver() {
	}

	public dynamic function onOut() {
	}

	public dynamic function onClick() {
	}

	public dynamic function onPush() {
	}

	public dynamic function onKeyDown(k : Int) {
	}

	public dynamic function onUnroll() {
	}

	public function focus() {
		interactive.focus();
	}

	public function blur() {
		interactive.blur();
	}

	public function showHover() {
		hideHover();
		for (e in hoverTile)
			currentBatch.add(e);
	}

	public function hideHover() {
		for (e in hoverTile)
			e.remove();
	}

	public function showActive() {
		background.removeChildren();
		background.addChild(activeTile);
		currentBatch = activeTile;

		if (dom.hover)
			showHover();
	}

	public function showBase() {
		background.removeChildren();
		background.addChild(baseTile);
		currentBatch = baseTile;

		if (dom.hover)
			showHover();
	}

	public function setVerticalMargin(m : Float) {
		marginUp = marginDown = m;
		updateBounds();
	}

	public function setHorizontalMargin(m : Float) {
		marginLeft = marginRight = m;
		updateBounds();
	}

	private function updateBounds() {
		var w = Std.int(width + marginLeft + marginRight);
		var h = Std.int(height + marginUp + marginDown);
		var bTile = h2d.Tile.fromColor(0xffffff, w, h, 0.);
		boundTile.tile = bTile;
		background.setPosition(marginLeft, marginUp);
		interactive.setPosition(marginLeft, marginUp);
		updateText();
	}

	private function updateText() {
		labelTxt.x = Std.int(width/2 - labelTxt.textWidth/2) + marginLeft;
		labelTxt.y = Std.int(height/2 - labelTxt.textHeight/2) + marginUp;
	}

	public function resize(w, h) {
		this.width = w;
		this.height = h;

		this.interactive.width = w;
		this.interactive.height = h;

		updateBounds();
		syncScale();
	}
}

@:uiComp("button")
class ButtonComp extends InteractiveWidgetComp {
	static var SRC = <button>
	</button>

	public function new( t: WidgetSettings, ?parent ) {
		super(t, parent);

		initComponent();
	}
}

@:uiComp("checkbox")
class CheckboxComp extends InteractiveWidgetComp {

	public var isChecked(default, null) : Bool = false;

	public function setIsChecked(b : Bool) {
		isChecked = b;
		showState();
	}

	static var SRC = <checkbox>
	</checkbox>

	public function new( t: WidgetSettings, ?parent ) {
		super(t, parent);

		initComponent();
	}

	override public function over() {
		dom.hover = true;
		showHover();
		onOver();
	}


	override public function out() {
		dom.hover = false;
		showState();
		hideHover();
		onOut();
	}

	public function showState() {
		if (isChecked)
			showActive();
		else
			showBase();
	}
}

@:uiComp("text-field")
class TextFieldComp extends InteractiveWidgetComp implements tools.MenuControl.Control {
	static var SRC = <text-field></text-field>;

	public var align(never, set) : h2d.Text.Align;
	inline function set_align(a : h2d.Text.Align) {
		switch (a) {
			case h2d.Text.Align.Center : textinput.x = width/2;
			default : textinput.x = 0;
		}
		return textinput.textAlign = a;
	}

	public var text(never, set) : String;
	var textinput : h2d.TextInput;
	inline function set_text(s : String) return textinput.text = s;

	public var maxWidth(never, set) : Null<Float>;
	inline function set_maxWidth(f : Null<Float>) return textinput.maxWidth = f;

	var control : tools.CustomControl;

	public function new(control : tools.CustomControl, s : WidgetSettings, ?parent) {
		super(s, parent);

		this.control = control;

		initComponent();

		textinput = new h2d.TextInput(Settings.uiFont, this);

		onClick = () -> {
			textinput.focus();
			onFocus();
			showActive();

			switch menu {
				case Some (m): m.lock(this);
				case None:
			}
		};

		textinput.onFocusLost = (_) -> {
			onFocusLost(textinput.text);
			showBase();
		};

		textinput.onChange = () -> {
			textinput.text = textinput.text.substr(0, 20);
			this.onChange();
		}
	}

	public dynamic function onFocusLost(txt : String) {
	}

	public dynamic function onFocus() {
	}

	public function clear() {
		onFocus = () -> return;
		onFocusLost = (_) -> return;
		textinput.text = "";
	}

	public dynamic function onChange() {
	}

	public function update() {
		if(control.probe(Exit)) {
			switch (menu) {
				case Some (m):
					tools.CustomControl.suspend(0.3);
					m.unlock();
				case None:
			}
		}
	}

	public function unlock() {
	}

	override public function resize(w, h) {
		super.resize(w, h);

		align = textinput.textAlign;
	}

	override public function rollBack() {
		switch (menu) {
			case Some (m): m.unlock();
			case None:
		}
	}
}

@:uiComp("anchor")
class AnchorComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <anchor></anchor>;

	public function new(?parent) {
		super(parent);

		initComponent();
	}
}

@:uiComp("container")
class ContainerComp extends h2d.Flow implements h2d.domkit.Object {
	static var SRC = <container></container>;

	public function new(?parent) {
		super(parent);

		initComponent();
	}
}

class OrbitalElement {
	public var shouldDraw : Bool = true;
	public var me : h2d.Object;

	public function new(me) {
		this.me = me;
	}
}

@:uiComp("orbital-container")
class OrbitalContainerComp extends h2d.Object implements h2d.domkit.Object {
	static var SRC = <orbital-container></orbital-container>;

	var level : Int;

	var graphics : h2d.Graphics;
	var subAnchor : h2d.Object;
	var subs : Array<OrbitalElement> = new Array();
	var color : Int;

	public static var uiscale = 1;
	static var radius = 32 * 2 * uiscale;
	static var lineLength = 32 * uiscale;
	static public var circleRadius(get, never) : Float; inline static function get_circleRadius() return (radius - lineLength)/2;

	public function new(c : Int, level : Int, ?parent) {
		super(parent);

		this.level = level;
		color = c;

		initComponent();

		subAnchor = new h2d.Object(this);

		drawInterface();
	}

	private function drawInterface() {
		if (graphics != null)
			graphics.remove();

		graphics = new h2d.Graphics(this);
		graphics.lineStyle(3, color);

		for (oe in subs) {
			if (oe.shouldDraw) {
				var c = oe.me;
				var angle = Math.atan2(c.y, c.x);
				graphics.moveTo(circleRadius * Math.cos(angle), circleRadius * Math.sin(angle));
				graphics.lineTo((circleRadius + lineLength) * Math.cos(angle), (circleRadius + lineLength)* Math.sin(angle));
			}
		}
	}

	public function addOrbitalChild(oe : OrbitalElement, offsetAngle : Float){
		var c = oe.me;
		this.subAnchor.addChild(c);
		this.subs.push(oe);

		var m = this.subAnchor.numChildren;

		for (i in 0...this.subAnchor.numChildren) {
			var angle;

			if (level == 0)
				angle = -2*Math.PI/3 + 2*Math.PI * i/m;
			else {
				var allowedAngle = 4*Math.PI/5;
				angle = offsetAngle - allowedAngle/2 + allowedAngle * (i + 1) / m - allowedAngle / (2 * m);
			}

			var c = this.subAnchor.getChildAt(i);
			if (this.subs[i].shouldDraw) {
				setPosPolar(c, radius, angle);
			} else {
				setPosPolar(c, 0, angle);
			}
		}

		drawInterface();
	}

	static private function setPosPolar(c : h2d.Object, radius : Float, angle : Float) {
		c.x = radius * Math.cos(angle);
		c.y = radius * Math.sin(angle);
	}

	public function resize() {
		radius = 32 * 2 * uiscale;
		lineLength = 32 * uiscale;

		drawInterface();
	}
}

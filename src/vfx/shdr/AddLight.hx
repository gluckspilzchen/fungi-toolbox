/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class AddLight extends hxsl.Shader {
    static var SRC = {
        @:import h3d.shader.Base2d;

        @param var bgTexture : Sampler2D;
        @param var lightTexture : Sampler2D;
        @param var brightnessTexture : Sampler2D;
		@global var valueTexture : Sampler2D;
		@global var normals : Sampler2D;
		@param var seed : Int;

		function randSeedCoords(seed:Int, x:Int, y:Int, max:Int) : Int {
			// Source: https://stackoverflow.com/questions/37128451/random-number-generator-with-x-y-coordinates-as-seed
			var h = seed + x*374761393 + y*668265263; // all constants are prime
			h = (h^(h >> 13)) * 1274126177;
			return ( h^(h >> 16) ) % max;
		}

		function getColor(light_color : Vec4, dark_color : Vec4, value : Float, brightness : Float) : Vec4 {
			if (value >= brightness) {
				return dark_color;
			}  else {
				var threshold = clamp((value / brightness - 0.4 ) * 2.5, 0., 1.);
				var r = float(randSeedCoords(seed, int(fragCoord.x), int(fragCoord.y), 255))/float(255);
				if ( pow(r, 1./3) > threshold ) {
					return light_color;
				} else {
					return dark_color;
				}
			}
		}

        function fragment() {
            var coord = calculatedUV;

            var light = lightTexture.get(coord);
            var bg = bgTexture.get(coord);
			var brightness = brightnessTexture.get(coord);
			var value = valueTexture.get(coord);
			var normal = normals.get(coord);

			output.color = getColor(light+bg, bg, value.x, brightness.x);
//			output.color = normal;
//			output.color = brightness;
        }
    }


    public function new(t : h3d.mat.Texture, lt : h3d.mat.Texture, bt : h3d.mat.Texture) {
        super();

        bgTexture = t;
        bgTexture.filter = Nearest;

        lightTexture = lt;
        lightTexture.filter = Nearest;

        brightnessTexture = bt;
        brightnessTexture.filter = Nearest;
    }
}

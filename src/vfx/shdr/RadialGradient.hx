/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class RadialGradient extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@global var screenSize : Vec2;

        @param var innerColor : Vec4;
        @param var outerColor : Vec4;

		@param var center : Vec2;
		@param var radius : Float;

        function fragment() {
			var pos = ((outputPosition.xy + vec2(1)) * 0.5) * screenSize;

			var dir = center - pos;
			var dist = length(dir);


			var coeff = clamp(dist/radius, 0.0, 1.0);

			output.color = mix(innerColor, outerColor, coeff * coeff);
		}
	}

	public function new() {
		super();
	}
}

/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024-2025 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class AmbientLight extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@global var offset : Vec2;
		@global var resolution : Vec2;
		@global var screenSize : Vec2;

		@global var normals : Sampler2D;
		@global var noise : Sampler2D;
        @global var vertical : Vec4;

		@param var data : Vec4;
        @param var color : Vec4;

		var pixelBrightness : Vec4;

		function pixelise(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			var nx = (1/tdiff)*x-(t0/tdiff);
			nx = floor(nx * resolution) / resolution;
			return (nx + (t0/tdiff)) * tdiff;
		}

        function fragment() {
			var pos = ((outputPosition.xy + vec2(1)) * 0.5);
			pos = pixelise(pos) * screenSize;
			var coord = ((outputPosition.xy + vec2(1)) * 0.5);

			var strength = data.w;

			var normal = normals.get(coord);
			var pixelnormal = 2*normal.xyz - vec3(1);

			var toLight = normalize(data.xyz);

			if (normal.w == 0) {
				pixelBrightness = vec4(0,0,0,1);
				output.color = vec4(0);
			} else if (normal.x == vertical.x && normal.y == vertical.y) {
				var brightness = strength * 0.7 * normal.z;

				output.color = color * brightness;
				pixelBrightness = vec4(vec3(brightness), 1);
			} else {
				var brightness = clamp(dot(toLight, pixelnormal), 0.0, 1.0);
				brightness *= strength * 0.7;

				output.color = color * brightness;
				pixelBrightness = vec4(vec3(brightness), 1);
			}
        }
    }


	public function new() {
		super();
	}
}

/*
    FungiToolBox, helpful classes to make video games.
    Copyright (C) 2024 Quentin Lambert
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/
package vfx.shdr;

class Heat extends hxsl.Shader {
    static var SRC = {
		@:import h3d.shader.Base2d;

		@global var offset : Vec2;
		@global var resolution : Vec2;
		@global var screenSize : Vec2;

		@param var center : Vec2;
        @global var noise : Sampler2D;
		@param var noiseSize : Vec2;
		@param var radius : Float;
		@param var strength : Vec2;
		@param var speed : Vec2;
		@global var bgTexture : Sampler2D;
		@param var textureSize : Vec2;

		function toTexture(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			return (1/tdiff)*x-(t0/tdiff);
		}

		function pixelise(x : Vec2) : Vec2 {
			var t0 = offset/screenSize;
			var t1 = (offset+resolution)/screenSize;
			var tdiff = t1 - t0;

			var nx = (1/tdiff)*x-(t0/tdiff);
			nx = floor(nx * resolution) / resolution;
			return (nx + (t0/tdiff)) * tdiff;
		}

        function fragment() {
			var base = (outputPosition.xy + vec2(1)) * 0.5;
			var noffset = vec2(0);
			noffset.x = floor(noiseSize.x * sign((time * speed.x)))/noiseSize.x;
			noffset.y = floor(noiseSize.y * time * speed.y)/noiseSize.y;
			var ncoord = toTexture(base + noffset);
			var noise = noise.get(ncoord);

			var pos = pixelise(base);
			var v = pos * screenSize - (center + offset);
			var r = (v.x*v.x + v.y*v.y);

			if (r > radius * radius)
				discard;

			//TODO this doesn't tile well, it should be a max
			var nv = v / radius;
			var r = 1 - sqrt(nv.x*nv.x + nv.y*nv.y);
			var f = smoothstep(0, 1, r);

			var coord = (outputPosition.xy + vec2(1)) * 0.5;
			var coffset = floor(f * strength * ((noise.xy * 2) - vec2(1)) * textureSize ) / textureSize;
			coord += coffset;
			coord = pixelise(coord);

			output.color = bgTexture.get(coord);
			output.color.w = 1;
        }
    }


	public function new(r : Float, centerX : Float, centerY : Float) {
		super();

		center.x = centerX;
		center.y = centerY;

		strength = new h3d.Vector(.0025, 0.004);
		speed = new h3d.Vector(1,.06);
		radius = r;

		noiseSize = new h3d.Vector(Assets.noiseTexture.width, Assets.noiseTexture.height, 0);
		textureSize = new h3d.Vector(Boot.ME.renderTarget.width, Boot.ME.renderTarget.height, 0);
	}
}
